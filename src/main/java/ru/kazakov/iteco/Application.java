package ru.kazakov.iteco;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.view.ConsoleWriter;

public class Application {

    public static void main(String[] args) throws Exception {
        new ConsoleWriter().showManagerOpened();
        new Bootstrap().start();
    }

}
