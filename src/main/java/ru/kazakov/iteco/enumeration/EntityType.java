package ru.kazakov.iteco.enumeration;

public enum EntityType {

    PROJECT ("project"),
    TASK ("task");

    private String value = "";

    EntityType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
