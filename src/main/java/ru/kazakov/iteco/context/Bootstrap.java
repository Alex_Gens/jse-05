package ru.kazakov.iteco.context;

import ru.kazakov.iteco.command.*;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.service.ProjectService;
import ru.kazakov.iteco.service.TaskService;
import ru.kazakov.iteco.view.ConsoleWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectService projectService = new ProjectService(projectRepository ,taskRepository);
    private final ConsoleReader reader = new ConsoleReader();
    private final ConsoleWriter writer = new ConsoleWriter();

    {
        registry(new ProjectCreateCommand(this));
        registry(new ProjectGetCommand(this));
        registry(new ProjectUpdateCommand(this));
        registry(new ProjectRemoveCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectAddTaskCommand(this));
        registry(new ProjectListTasksCommand(this));
        registry(new ProjectClearCommand(this));
        registry(new TaskCreateCommand(this));
        registry(new TaskGetCommand(this));
        registry(new TaskUpdateCommand(this));
        registry(new TaskRemoveCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskClearCommand(this));
        registry(new HelpCommand(this));
        registry(new ExitCommand(this));
    }


    public void start() throws Exception {
        String command = "";
        while (true) {
            command = reader.enterIgnoreEmpty();
            command = command.trim().toLowerCase();
            execute(command);
        }
    }

    private void registry(AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public ProjectService getProjectService() {
        return projectService;
    }

    public List<AbstractCommand> getCommands() { return new ArrayList<>(commands.values());}

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (!commands.containsKey(command)) {
            writer.showCommandNotExist();
            writer.separateLines();
            return;
        }
        abstractCommand.execute();
    }

}
