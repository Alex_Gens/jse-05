package ru.kazakov.iteco.repository;

import ru.kazakov.iteco.entity.Project;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository extends AbstractRepository<Project> {

    private Map<String, Project> entities = new LinkedHashMap<>();

    public String getName(String id) {
        return entities.get(id).getName();
    }

    public void setName(String name, String id) {
        entities.get(id).setName(name);
    }

    @Override
    public void merge(Project entity) {
        entities.merge(entity.getId(), entity, (v1, v2)
                -> {String newInfo = v1.getInfo() + v2.getInfo();
                    entity.setInfo(newInfo);
                    return entity;});
    }

    @Override
    public void persist(Project entity) {
        entities.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public void remove(String id) {
        entities.remove(id);
    }

    public void remove(List<String> ids) {
        entities.entrySet().removeIf(entry
                -> ids.contains(entry.getValue().getId()));
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public Project findOne(String id) {
        return entities.get(id);
    }

    @Override
    public List<Project> findAll() {
        List<Project> projects = new ArrayList<>(entities.values());
        return projects;
    }

    public List<Project> findAll(List<String> ids) {
        List<Project> values = (List<Project>) entities.values();
        values.removeIf(v -> !ids.contains(v.getId()));
        return values;
    }

    public boolean isEmpty() {
        return entities.isEmpty();
    }

    public boolean isEmpty(String id) {
        return entities.get(id).isEmpty();
    }

}
