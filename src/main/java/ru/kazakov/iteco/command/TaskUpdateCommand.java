package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.util.ConsoleReader;

public class TaskUpdateCommand extends TaskAbstractCommand {

    private final ConsoleReader reader = new ConsoleReader();

    private final String name = "task-update";

    private final String description = "Update task information.";

    public TaskUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Task task = taskService.findOne(name, true);
        writer.showFinishAndSave();
        writer.showEnterInformation(entityType);
        String newInfo = reader.read();
        task.setInfo(newInfo);
        writer.showActionInBreakets(Action.UPDATED);
        writer.showSuccessfullyDone(entityType, Action.UPDATED);
        writer.separateLines();
    }

}
