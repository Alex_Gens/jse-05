package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.Action;

public class ProjectCreateCommand extends ProjectAbstractCommand {

    private final String name = "project-create";

    private final String description = "Create new project.";

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() { return description;}

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        String name = writer.enterEntityName(entityType);
        if (projectService.contains(name)) {
            writer.showNotActionInBreakets(Action.CREATED);
            writer.showNameIsExist(entityType);
            writer.separateLines();
            return;
        }
        projectService.persist(name);
        writer.showActionInBreakets(Action.CREATED);
        writer.showSuccessfullyDone(entityType, Action.CREATED);
        writer.separateLines();
    }

}
