package ru.kazakov.iteco.command;

import static ru.kazakov.iteco.enumeration.Action.ADDED;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.TaskService;

public class ProjectAddTaskCommand extends ProjectAbstractCommand {

    private final String name = "project-add-task";

    private final String description = "Add task to project.";

    public ProjectAddTaskCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String projectName = writer.enterEntityName(entityType);
        if (!projectService.contains(projectName)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        TaskService taskService = bootstrap.getTaskService();
        String taskName = writer.enterEntityName(EntityType.TASK);
        if (!taskService.contains(taskName)) {
            writer.showNameNotExist(EntityType.TASK);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(projectName, true);
        Task task = taskService.findOne(taskName, true);
        if (task.getProject().equals(project.getId())) {
            writer.showNameIsAlreadyAdded(EntityType.TASK);
            writer.separateLines();
            return;
        }
        task.setProject(project.getId());
        writer.showSuccessfullyDone(EntityType.TASK, ADDED);
        writer.separateLines();
    }

}
