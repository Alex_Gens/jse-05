package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.ProjectService;
import ru.kazakov.iteco.view.ConsoleWriter;

public abstract class ProjectAbstractCommand extends AbstractCommand {

    final EntityType entityType = EntityType.PROJECT;

    final ConsoleWriter writer = new ConsoleWriter();

    final ProjectService projectService = bootstrap.getProjectService();

    public ProjectAbstractCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

}
