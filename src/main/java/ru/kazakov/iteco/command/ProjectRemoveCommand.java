package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.service.TaskService;
import java.util.ArrayList;
import java.util.List;

public class ProjectRemoveCommand extends ProjectAbstractCommand {

    private final String name = "project-remove";

    private final String description = "Remove project.";

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(name, true);
        TaskService taskService = bootstrap.getTaskService();
        List<String> taskIds = new ArrayList<>();
        for ( Task task : taskService.findAll()
        ) {
            if (task.getProject().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        taskService.remove(taskIds);
        projectService.remove(project.getId());
        writer.showActionInBreakets(Action.REMOVED);
        writer.showSuccessfullyDone(entityType, Action.REMOVED);
        writer.separateLines();
    }

}
