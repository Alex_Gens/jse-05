package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;

public class TaskClearCommand extends TaskAbstractCommand {

    private final String name = "task-clear";

    private final String description = "Remove all tasks.";

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        taskService.removeAll();
        writer.showClear(entityType);
        writer.separateLines();
    }

}
