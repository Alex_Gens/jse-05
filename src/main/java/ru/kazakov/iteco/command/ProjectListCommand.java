package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;

import java.util.List;

public class ProjectListCommand extends ProjectAbstractCommand {

    private final String name = "project-list";

    private final String description = "Show all projects.";

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        if (projectService.isEmpty()) {
            writer.showListIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        int counter = 1;
        writer.showListInBreakets(entityType);
        List<Project> projects = projectService.findAll();
        for (Project project : projects) {
            writer.printListValue(counter, project.getName());
            counter++;
        }
        writer.separateLines();
    }

}
