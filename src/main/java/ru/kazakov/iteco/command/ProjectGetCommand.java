package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;

public class ProjectGetCommand extends ProjectAbstractCommand {

    private final String name = "project-get";

    private final String description = "Show all project information.";

    public ProjectGetCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(name, true);
        if (projectService.isEmpty(project.getId())) {
            writer.showInfoIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        writer.showNameInBreakets(entityType, name);
        writer.printInformation(project.getInfo());
        writer.separateLines();
    }

}
