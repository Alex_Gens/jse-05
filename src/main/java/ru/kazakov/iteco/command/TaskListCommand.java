package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;

import java.util.List;

public class TaskListCommand extends TaskAbstractCommand {

    private final String name = "task-list";

    private final String description = "Show all tasks.";

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        if (taskService.isEmpty()) {
            writer.showListIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        int counter = 1;
        writer.showListInBreakets(entityType);
        List<Task> tasks = taskService.findAll();
        for (Task task : tasks) {
            writer.printListValue(counter, task.getName());
            counter++;
        }
        writer.separateLines();
    }

}
