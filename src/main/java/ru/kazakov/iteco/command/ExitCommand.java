package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.view.ConsoleWriter;

public class ExitCommand extends AbstractCommand {

    private final String name = "exit";

    private final String description = "Close task manager.";

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        new ConsoleWriter().showManagerClosed();
        System.exit(0);
    }

}
