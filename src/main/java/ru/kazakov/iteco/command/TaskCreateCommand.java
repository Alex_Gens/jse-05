package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.enumeration.EntityType;

public class TaskCreateCommand extends TaskAbstractCommand {

    private final String name = "task-create";

    private final String description = "Create new task.";

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (taskService.contains(name)) {
            writer.showNotActionInBreakets(Action.CREATED);
            writer.showNameIsExist(entityType);
            writer.separateLines();
            return;
        }
        taskService.persist(name);
        writer.showActionInBreakets(Action.CREATED);
        writer.showSuccessfullyDone(entityType, Action.CREATED);
        writer.separateLines();
    }

}
