package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.TaskService;
import java.util.ArrayList;
import java.util.List;

public class ProjectListTasksCommand extends ProjectAbstractCommand {

    private final String name = "project-list-tasks";

    private final String description = "Show all tasks in project.";

    public ProjectListTasksCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        EntityType entityType = EntityType.PROJECT;
        String projectName = writer.enterEntityName(entityType);
        if (!projectService.contains(projectName)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        TaskService taskService = bootstrap.getTaskService();
        Project project = projectService.findOne(projectName, true);
        List<String> taskIds = new ArrayList<>();
        for ( Task task : taskService.findAll()
        ) {
            if (task.getProject().equals(project.getId())) {
                taskIds.add(task.getId());
            }
        }
        if (taskIds.isEmpty()) {
            writer.showTaskListIsEmpty();
            writer.separateLines();
            return;
        }
        int counter = 1;
        writer.showListInBreakets(EntityType.TASK);
        for (String taskId : taskIds
        ) {
            writer.printListValue(counter, taskService.getName(taskId));
            counter++;
        }
        writer.separateLines();
    }

}
