package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.enumeration.EntityType;

public class TaskRemoveCommand extends TaskAbstractCommand {

    private final String name = "task-remove";

    private final String description = "Remove task.";

    public TaskRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Task task = taskService.findOne(name, true);
        taskService.remove(task.getId());
        writer.showActionInBreakets(Action.REMOVED);
        writer.showSuccessfullyDone(entityType, Action.REMOVED);
        writer.separateLines();
    }

}
