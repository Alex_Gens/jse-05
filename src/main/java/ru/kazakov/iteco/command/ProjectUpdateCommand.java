package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.util.ConsoleReader;

public class ProjectUpdateCommand extends ProjectAbstractCommand {

    private final ConsoleReader reader = new ConsoleReader();

    private final String name = "project-update";

    private final String description = "Update project information.";

    public ProjectUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        String name = writer.enterEntityName(entityType);
        if (!projectService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Project project = projectService.findOne(name, true);
        writer.showFinishAndSave();
        writer.showEnterInformation(entityType);
        String newInfo = reader.read();
        project.setInfo(newInfo);
        writer.showActionInBreakets(Action.UPDATED);
        writer.showSuccessfullyDone(entityType, Action.UPDATED);
        writer.separateLines();
    }

}
