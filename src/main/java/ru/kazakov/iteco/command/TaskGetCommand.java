package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.EntityType;

public class TaskGetCommand extends TaskAbstractCommand {

    private final String name = "task-get";

    private final String description = "Show all task information.";

    public TaskGetCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() throws Exception {
        EntityType entityType = EntityType.TASK;
        String name = writer.enterEntityName(entityType);
        if (!taskService.contains(name)) {
            writer.showNameNotExist(entityType);
            writer.separateLines();
            return;
        }
        Task task = taskService.findOne(name, true);
        if (taskService.isEmpty(task.getId())) {
            writer.showInfoIsEmpty(entityType);
            writer.separateLines();
            return;
        }
        writer.showNameInBreakets(entityType, name);
        writer.printInformation(task.getInfo());
        writer.separateLines();
    }

}
