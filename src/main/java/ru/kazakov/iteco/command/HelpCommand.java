package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.view.ConsoleWriter;

public class HelpCommand extends AbstractCommand {

    private final ConsoleWriter writer = new ConsoleWriter();

    private final String name = "help";

    private final String description = "Show all commands.";

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setBootStrap(Bootstrap bootStrap) {
        this.bootstrap = bootStrap;
    }

    @Override
    public void execute() {
        writer.showHelp(bootstrap);
    }

}
