package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;

public abstract class AbstractCommand {

    Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap){
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void setBootStrap(Bootstrap bootStrap);

    public abstract void execute() throws Exception;

}
