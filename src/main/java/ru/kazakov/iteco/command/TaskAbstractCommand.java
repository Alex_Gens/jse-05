package ru.kazakov.iteco.command;

import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.service.TaskService;
import ru.kazakov.iteco.view.ConsoleWriter;

public abstract class TaskAbstractCommand  extends AbstractCommand {

    final EntityType entityType = EntityType.TASK;

    final ConsoleWriter writer = new ConsoleWriter();

    final TaskService taskService = bootstrap.getTaskService();

    public TaskAbstractCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

}
