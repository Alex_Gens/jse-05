package ru.kazakov.iteco.view;

import ru.kazakov.iteco.command.AbstractCommand;
import ru.kazakov.iteco.context.Bootstrap;
import ru.kazakov.iteco.enumeration.Action;
import ru.kazakov.iteco.enumeration.EntityType;
import ru.kazakov.iteco.util.ConsoleReader;
import ru.kazakov.iteco.util.Format;
import java.io.IOException;
import java.util.List;

public class ConsoleWriter {

    public final String separator = System.lineSeparator();

    public String enterEntityName(EntityType entityType) throws IOException {
        showEnterName(entityType);
        ConsoleReader consoleReader = new ConsoleReader();
        String result = consoleReader.enterIgnoreEmpty();
        return result;
    }

    public void showEnterName(EntityType entityType) {
        System.out.println("ENTER " + entityType.getValue().toUpperCase() + " NAME:");
    }

    public void showActionInBreakets(Action action) {
        System.out.println("[" + action + "]");
    }

    public void showNotActionInBreakets(Action action) {
        System.out.println("[NOT " + action + "]");
    }

    public void showNameInBreakets(EntityType entityType, String name) {
        System.out.println("[" + entityType.getValue().toUpperCase() + ": " + name + "]");
    }

    public void showNameIsExist(EntityType entityType) {
        System.out.println(Format.firstUpperCase(entityType.getValue()) + " with this name is already exists. Use another " + entityType.getValue() + " name.");
    }

    public void showNameIsAlreadyAdded(EntityType entityType) {
        System.out.println(Format.firstUpperCase(entityType.getValue()) + " is already added. Use project-list-tasks to see " + entityType.getValue() +
                "s in project.");
    }

    public void showNameNotExist(EntityType entityType) {
        System.out.println(Format.firstUpperCase(entityType.getValue()) + " with this name doesn't exist. Use \"" + entityType.getValue() +  "-list\" to show all projects.");
    }

    public void showSuccessfullyDone(EntityType entityType, Action action) {
        System.out.println(Format.firstUpperCase(entityType.getValue()) + " successfully " + action.toString().toLowerCase() + "!");
    }

    public void showInfoIsEmpty(EntityType entityType) {
        System.out.println(Format.firstUpperCase(entityType.getValue()) + " is empty. Use \"" + entityType.getValue() +  "-update\" to update this project.");
    }

    public void showEnterInformation(EntityType entityType) {
        System.out.println("ENTER " + entityType.getValue().toUpperCase() + " INFORMATION");
    }

    public void showFinishAndSave() {
        System.out.println("Use \"-save\" to finish entering, and save information.");
    }

    public void showListIsEmpty(EntityType entityType) {
        System.out.println(Format.firstUpperCase(entityType.getValue()) + " list is empty. Use \"" + entityType.getValue() +  "-create\" to create " + entityType.getValue() +  ".");
    }

    public void showTaskListIsEmpty() {
        System.out.println("The project has no tasks. Use project-add-task to add task to project.");
    }

    public void showListInBreakets(EntityType entityType) {
        System.out.println("[" + entityType.getValue().toUpperCase() + "S LIST]");
    }

    public void showClear(EntityType entityType) {
        System.out.println("[ALL " + entityType.getValue().toUpperCase() + "S REMOVED]" + separator + "" + Format.firstUpperCase(entityType.getValue()) +
                "s successfully removed!");
    }

    public void showCommandNotExist() {
    System.out.println("Command doesn't exist. Use \"help\" to show all commands.");
    }

    public void showHelp(Bootstrap bootstrap) {
        List<AbstractCommand> commands = bootstrap.getCommands();
        commands.forEach(v -> System.out.println(v.getName() + ": " + v.getDescription()));
        separateLines();
    }

    public void showManagerOpened() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    public void showManagerClosed() {
        System.out.println("*** MANAGER CLOSED ***");
    }

    public void printListValue(int counter, String value) {
        System.out.println(counter + ". " + value);
    }

    public void printInformation(String information) {
        System.out.println(information);
    }

    public void separateLines() {
        System.out.print(separator);
    }

}
