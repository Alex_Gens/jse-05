package ru.kazakov.iteco.entity;

import ru.kazakov.iteco.api.IManageable;
import java.util.Date;
import java.util.UUID;

public class Task implements IManageable {

    private final String id = UUID.randomUUID().toString();

    private String name = "";

    private String info = "";

    private Date dateStart;

    private Date dateFinish;

    private String project = "";

    public Task() {}

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Date getDateStart() {
        return dateStart;
    }

    @Override
    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    @Override
    public String getInfo() {
        return info;
    }

    @Override
    public void setInfo(String info) {
        this.info = info;
    }

    public boolean isEmpty() {
        return info == null || info.isEmpty();
    }

}
