package ru.kazakov.iteco.util;

public class Format {

    public static String firstUpperCase(String str) {
        String result = "";
        if (str != null && !str.isEmpty()) {
            result = str.substring(0, 1).toUpperCase() + str.substring(1);
        }
        return result;
    }

}
