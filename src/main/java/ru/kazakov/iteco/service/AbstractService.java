package ru.kazakov.iteco.service;

import java.util.List;

public abstract class AbstractService<T> {

    public abstract T findOne(String id) throws Exception;

    public abstract List<T> findAll();

    public abstract void merge(T entity) throws Exception;

    public abstract void persist(T entity) throws Exception;

    public abstract void remove(String id) throws Exception;

    public abstract void removeAll();

    public abstract boolean isEmpty();

}
